﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int sum = 0;
                String T1;
                String[] Temp1;

                Console.Write("Please enter the line: ");

                T1 = Console.ReadLine().Remove(0, 18).Trim();
                if (T1 == "")
                {
                    sum = 0;
                }
                else
                {
                    Temp1 = T1.Split(',');
                    for (int i = 0; i < Temp1.Count(); i++)
                    {
                        if (i == 0)
                            sum = Convert.ToInt32(Temp1[i]);
                        else
                            sum = sum + Convert.ToInt32(Temp1[i]);
                    }
                }
                Console.WriteLine(sum.ToString());
                Console.ReadLine();
            }
            catch
            {
                Console.WriteLine("Please enter value in correct format!");
                Console.ReadLine();
            }
        }
    }
}
