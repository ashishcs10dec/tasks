﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int num1;
                int num2;
                string operand;
                float answer;
                String T1;
                String[] Temp1;

                Console.Write("Please enter the line: ");

                T1 = Console.ReadLine().Remove(0, 18).Trim();
                if (T1 == "")
                {
                    num1 = 0;
                    num2 = 0;
                }
                else
                {
                    Temp1 = T1.Split(',');
                    if (Temp1.Count() == 2)
                    {
                        num1 = Convert.ToInt32(Temp1[0]);
                        num2 = Convert.ToInt32(Temp1[1]);
                    }
                    else if (Temp1.Count() == 1)
                    {
                        num1 = Convert.ToInt32(Temp1[0]);
                        num2 = 0;
                    }
                    else
                    {
                        num1 = 0;
                        num2 = 0;
                    }
                }
                operand = "+";
                switch (operand)
                {
                    case "+":
                        answer = num1 + num2;
                        break;
                    default:
                        answer = 0;
                        break;
                }
                Console.WriteLine(answer.ToString());
                Console.ReadLine();
            }
            catch
            {
                Console.WriteLine("Please enter value in correct format!");
                Console.ReadLine();
            }
        }
    }
}
