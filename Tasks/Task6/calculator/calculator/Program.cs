﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculator
{
    class Program
    {
        //Also show the negative numbers in error message. For example, output of 
        //calculator.exe add \\,\\2,7,-3,5,-2 
        //should be:
        //Error: Negative numbers (-3,-2) not allowed.

        static void Main(string[] args)
        {
            try
            {
                int sum = 0;
                String T1;
                String[] Temp1;
                String Temp2 = "";

                Console.Write("Please enter the line: ");

                T1 = Console.ReadLine().Remove(0, 18).Trim();
                T1 = T1.Replace("\\\\", "0");
                if (T1 == "")
                {
                    sum = 0;
                }
                else
                {
                    Temp1 = T1.Split(new string[] { "," }, StringSplitOptions.None);
                    for (int i = 0; i < Temp1.Count(); i++)
                    {
                        if (Convert.ToInt32(Temp1[i]) < 0)
                        {
                            if (Temp2 == "")
                                Temp2 = Temp1[i];
                            else
                                Temp2 = Temp2 + Temp1[i];
                        }
                    }
                }
                Console.WriteLine("Negative numbers(" + Temp2 + ") not allowed.");
                Console.ReadLine();
            }
            catch
            {
                Console.WriteLine("Please enter value in correct format!");
                Console.ReadLine();
            }
        }
    }
}
