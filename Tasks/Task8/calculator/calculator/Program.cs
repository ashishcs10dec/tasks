﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calculator
{
    class Program
    {
        //Ignore numbers above 1000. For example, command 
        //calculator.exe add 10,20,1010,20 
        //should return 50.

        static void Main(string[] args)
        {
            try
            {
                int sum = 0;
                String T1;
                String[] Temp1;

                Console.Write("Please enter the line: ");

                T1 = Console.ReadLine().Remove(0, 23).Trim();
                Temp1 = T1.Split(new string[] { "," }, StringSplitOptions.None);
                for (int i = 0; i < Temp1.Count(); i++)
                {
                    if (Convert.ToInt32(Temp1[i]) < 1000)
                    {
                        if (i == 0)
                            sum = Convert.ToInt32(Temp1[i]);
                        else
                            sum = sum * Convert.ToInt32(Temp1[i]);
                    }
                }
                Console.WriteLine(sum);
                Console.ReadLine();
            }
            catch
            {
                Console.WriteLine("Please enter value in correct format!");
                Console.ReadLine();
            }
        }
    }
}
